const key = "e5f83c51795746d4b0a122032232406"
const link = "http://api.weatherapi.com/v1/current.json?";

const root = document.getElementById("root");
const popup = document.getElementById("popup");
const textInput = document.getElementById("text-input");
const form = document.getElementById("form");

let store = {
  city: "Kemerovo",
  temperature: 0,
  localtime: "1970-01-01 00:00",
  isDay: 1,
  description: "",
  properties: {
    cloud: {},
    humidity: {},
    windSpeed: {},
    pressure: {},
    uvIndex: {},
    visibility: {},
  },
};

const fetchData = async () => {
  
  try {
    const query = localStorage.getItem("query") || store.city;
    const result = await fetch(`${link}key=${key}&q=${query}&aqi=no`);
    const data = await result.json();
    console.log(data);
    const {
      current: {
        cloud,
        temp_c: temperature,
        humidity,
        pressure_mb: pressure,
        uv: uvIndex,
        vis_km: visibilityKm,
        is_day: isDay,
        condition: { text: description },
        wind_kph: windSpeedKph,       
      },
      location: { name, localtime },
    } = data;

    store = {
      isDay,
      city: name,
      temperature,
      localtime,
      description: description,
      properties: {
        cloud: {
          title: "Облачность",
          value: `${cloud}%`,
          icon: "cloud.png",
        },
        humidity: {
          title: "Влажность",
          value: `${humidity}%`,
          icon: "humidity.png",
        },
        windSpeed: {
          title: "Скорость ветра",
          value: `${Math.floor(windSpeedKph / 3.6)} м/с`,
          icon: "wind.png",
        },
        pressure: {
          title: "Давление",
          value: `${pressure * 0.75} мм рт. ст.`,
          icon: "gauge.png",
        },
        uvIndex: {
          title: "УФИ-Индекс",
          value: `${uvIndex}`,
          icon: "uv-index.png",
        },
        visibility: {
          title: "Видимость",
          value: `${visibilityKm} км`,
          icon: "visibility.png",
        },
      },
    };
    renderComponent();
  } catch (err) {
    console.log(err);
  }
};

const getImage = (description) => {
  const value = description.toLowerCase();

  switch (value) {
    case "partly cloudy":
      return "partly.png";
    case "clear":
      return "clear.png";
    case "fog":
      return "fog.png";
    case "sunny":
      return "sunny.png";
    case "cloud":
      return "cloud.png";
    default:
      return "the.png";
  }
};

const renderProperty = (properties) => {
  return Object.values(properties)
    .map(({ title, value, icon }) => {
      return `<div class="property">
                <div class="property-icon">
                  <img src="./img/icons/${icon}" alt="">
                </div>
                <div class="property-info">
                  <div class="property-info__value">${value}</div>
                  <div class="property-info__description">${title}</div>
                </div>
              </div>`;
    })
    .join("");
};

const markup = () => {
  const { city, description, localtime, temperature, isDay, properties } =
    store;
  const containerClass = isDay === 1 ? "is-day" : "";
  return `<div class="container ${containerClass}">
            <div class="top">
              <div class="city">
                <div class="city-subtitle">Погода сегодня в...</div>
                <div class="city-title" id="city">
                <span>${city}</span>
                </div>
              </div>
              <div class="city-info">
                <div class="top-left">
                <img class="icon" src="./img/${getImage(description)}" alt="" />
                <div class="description">${description}</div>
              </div>            
              <div class="top-right">
                <div class="city-info__subtitle">Время - ${localtime}</div>
                <div class="city-info__title">${temperature}°</div>
              </div>
            </div>
          </div>
        <div id="properties">${renderProperty(properties)}</div>
      </div>`;
};

const togglePopupClass = () => {
  popup.classList.toggle("active");
};

const renderComponent = () => {
  root.innerHTML = markup();
  const city = document.getElementById("city");
  city.addEventListener("click", togglePopupClass);
};

const handleInput = (e) => {
  store = {
    ...store,
    city: e.target.value,
  };
};

const handleSubmit = (e) => {
  e.preventDefault();
  const value = store.city;
  if (!value) return null;
  localStorage.setItem("query", value);
  fetchData();
  togglePopupClass();
};

form.addEventListener("submit", handleSubmit);
textInput.addEventListener("input", handleInput);

setTimeout(togglePopupClass, 750);